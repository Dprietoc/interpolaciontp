<?php
class cPolinomio{
    var $aPolinomio = array();
    
    private function validarPolinomio($arrayCoeficientes, $cantidadElementos = -1){
        if(!is_array($arrayCoeficientes)){
            throw new Exception("El paramentro enviado no es un array");
        }
        if ($cantidadElementos>=0 && count($arrayCoeficientes)!=$cantidadElementos){
            throw new Exception("El grado del polinomio supera el requerido, se esperaba un polinomio de grado ". $cantidadElementos - 1);
        }
    }
    
    
    public function cPolinomio($arrayCoeficientes){
        try{
            $this->validarPolinomio($arrayCoeficientes);
            $this->aPolinomio = $arrayCoeficientes;
        }
        catch(Exception $e){
            throw $e;
        }
            
        
    }
    public function multiplicarEscalar($escalar){
        for($i=0;$i<count($this->aPolinomio);$i++){
            $this->aPolinomio[$i] = $this->aPolinomio[$i]*$escalar;
        }
    }
    public function multiplicarBinomio($aBinomio){
        try{
            $this->validarPolinomio($aBinomio,2);
            $this->aPolinomio[count($this->aPolinomio)] = $this->aPolinomio[count($this->aPolinomio)-1]* $aBinomio[1];
            for($i=count($this->aPolinomio)-2;$i>0;$i--){
                $this->aPolinomio[$i] = $this->aPolinomio[$i]* $aBinomio[0] + $this->aPolinomio[$i-1]*$aBinomio[1];
            }
            $this->aPolinomio[0] = $this->aPolinomio[0]* $aBinomio[0];
        }
        catch(Exception $e){
            throw $e;
        }
    }
    public function sumar($aPolinomio){
        try{
            $this->validarPolinomio($aPolinomio);
            if(count($this->aPolinomio)>count($aPolinomio)){
                $aSumando = &$aPolinomio;
                $aResultado = &$this->aPolinomio;
            }else{
                $aResultado = &$aPolinomio;
                $aSumando = &$this->aPolinomio;
            }
            for($i=0;$i<count($aSumando);$i++){
                $aResultado[$i] = $aResultado[$i] + $aSumando[$i];
            }
            $this->aPolinomio = $aResultado;
        }
        catch(Exception $e){
            throw $e;
        }
    }
    
    public function imprimir(){
        $polinomio = "";
        for($i=(count($this->aPolinomio)-1);$i>=0;$i--){
           
            if($i>1){
                $polinomio = $polinomio . $this->aPolinomio[$i]."x^".$i." + ";
            }elseif($i==1){
                $polinomio = $polinomio . $this->aPolinomio[$i]."x". " + ";
            }  else {
                $polinomio = $polinomio . $this->aPolinomio[$i];
            }
        }
        echo $polinomio;
    }
}
?>
