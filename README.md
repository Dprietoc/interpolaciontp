Introducción
=============

Se deberá desarrollar una aplicación que permita encontrar el polinomio interpolante
en función de los puntos dados, y a su vez evaluar dicho polinomio en cualquier punto
del intervalo de la interpolación.

La aplicación podrá ser implementada en cualquier lenguaje de programación siempre
y cuando sea sencillo y rápido de instalarse y ejecutarse en cualquier ordenador.
Únicamente se solicita que la aplicación pueda ejecutarse sobre Windows.