<?php
include_once 'iNewton.php';
$puntos = "";
$error = false;
if(isset($_POST['puntos'])){
    try{
        $puntos = $_POST['puntos'];
        $n = new iNewton();
        $n->resetearPuntos();
        $n->agregarPuntos($puntos);
        $n->calcularPolinomio();
        //var_dump($n->aj);
    }
    catch(Exception $e){
        $error = true;
    }
}


?>
<!DOCTYPE html>
<html>
    <head>
        <title>Metodo de Interpolacion de Newton - Gregory</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <h2>Puntos:</h2>
        <p>Escribir los pares ordenados separando las coordenadas con "," y los pares por ";"</p>
        <form action="index.php" method="post">
            <?php if($error){ echo '<div id="err">Error en las coordenadas</div>';}?>
            <input name="puntos" value="<?php echo $puntos;?>"> 
            <input type="submit" value="Calcular">
        </form>
        <h2>Coeficientes Ai:</h2>
        <?php
            if(!$error && $puntos !=""){
                for($i=0;$i<count($n->aj);$i++){
                    echo "A".$i." = ".$n->aj[$i]."<br>";
                }
            }
        ?>
        <h2>Pruebas;</h2>
        <?php
//            $p = new cPolinomio(array(3,2,5));
//            echo "(";
//            $p->imprimir();
//            echo ") * (x+1) = ";
//            //$p->sumar(array(1,1,1,1,1));
//            $p->multiplicarBinomio(array(1,1));
//            $p->imprimir();
//            /*(3 + 2x + 5x^2*+ 2x^3)*(-2+x)*/
//            /*$polinomio = $n->multiplicarPolinomio(array(3,2,5,2), array(-2,1));
//            var_dump($polinomio);*/
        ?>
    </body>
</html>
