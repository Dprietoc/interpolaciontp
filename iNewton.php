<?php
include_once 'cPolinomio.php';

class iNewton{
    //public $valoresX = array(0.1,2.5,4.3,4.5,5.3,7.8);
    //public $valoresY = array(19.6482687,33.6979167,40.0771527,41.55625,42.5859193,47.230336);
    public $valoresX = array(0,1,2,3);
    public $valoresY = array(1,1,2,5);
    public $diferenciasDivididas = array(1);
    private $dibujarArray = false;
    public $aj = array();
    public $polinomio;
    
    
//    public function multiplicarPolinomio($aPolinomio,$aBinomio){
////        $aMultiplicadoPorA = array();
////        $aMultiplicadoPorX = array(0);
////        $aResultado = array();
////        for($i=0;$i<count($aPolinomio);$i++){
////            
////            $aMultiplicadoPorA[$i]= $aPolinomio[$i]*$aBinomio[0] ;
////            $aMultiplicadoPorX[$i+1]= $aPolinomio[$i];
////        }
////        $aMultiplicadoPorA[count($aMultiplicadoPorX)-1] = 0;
////        for($i=0;$i<count($aMultiplicadoPorX);$i++){
////            $aResultado[$i] = $aMultiplicadoPorA[$i] + $aMultiplicadoPorX[$i];
////        }
////        return $aResultado;
//    }
    public function calcularSubPolinomio(){
        
    }
    public function calcularPolinomio(){
        $this->calcularTablaDiferenciasDivididas($this->valoresY, 1);
        $this->calcularCoeficientesAj();
        
        for($i=0;$i<count($this->aj);$i++){
            //Hacer la multiplicacion del aj por el polinomio generado recursivamente
            $array[$i] = $this->aj[$i];
        }
        
        
    }
    public function calcularCoeficientesAj(){
        if($this->dibujarArray){
            echo "<table><tr>";
        }
        for($i=0;$i<=count($this->valoresY)-1;$i++){
            if($this->dibujarArray){
                echo "<td>";
            }
            $a = $this->diferenciasDivididas[$i];
            $a = $a * $this->valoresY[0];
            $a = $a / $this->factorial($i);
            if($i!= 0){   
                $a = $a / pow(($this->valoresX[$i] - $this->valoresX[$i-1]),($i));
            }
            $this->aj[$i] = $a;

            if($this->dibujarArray){
                echo $this->aj[$i];
                echo "</td>";
            }        
        }
        if($this->dibujarArray){
            echo "</tr></table>";
        }
    }
    public function dibujarTablaDiferenciasDivididas(){
        $aux = $this->dibujarArray;
        $this->dibujarArray = true;
            echo "<table>";
            echo "<tr>";
            for($i=0;$i<count($this->valoresY);$i++){
                echo "<td>";
                echo $this->valoresY[$i];
                echo "</td>";
            }
            echo "</tr>";
        $this->calcularTablaDiferenciasDivididas($this->valoresY, 1);
        
        echo "</table>";
        $this->dibujarArray = $aux;
    }
    private function calcularDiferencias($aNumeros){
        if(count($aNumeros)<2){
            return null;
        }
        for($i=0;$i<count($aNumeros)-1;$i++){
            $aDiferencias[] = $aNumeros[$i+1] - $aNumeros[$i];
        }
        return $aDiferencias;
    }
    public function calcularTablaDiferenciasDivididas($numeros, $i=1){
        if(count($numeros)<2){
            return;
        }
        $diferencias = $this->calcularDiferencias($numeros);
        $this->diferenciasDivididas[$i] = $diferencias[0];

        if($this->dibujarArray){
            echo "<tr>";
            for($j=0;$j<count($diferencias);$j++){
                echo "<td>".$diferencias[$j]."</td>";
            }
            echo "</tr>";
        }
        $i++;
        $this->calcularTablaDiferenciasDivididas($diferencias, $i);
    }
    public function resetearPuntos(){
        $this->valoresX = array();
        $this->valoresY = array();
    }
    public function agregarPunto($X,$Y){
        //TO DO: Control de ingreso
        $this->valoresX[count($this->valoresX)] = $X;
        $this->valoresY[count($this->valoresY)] = $Y;
    }
    private function factorial ($n)
    {
        if ($n<=1) return 1;
        else return $n * $this->factorial($n-1);
    }
    public function agregarPuntos($cadena){
        $puntos = explode(";", $cadena);
        foreach($puntos as $p){
            $coords = explode(",",$p);
            if(count($coords)!=2){
                throw new Exception("Coordenadas invalidas");
            }
            $this->agregarPunto($coords[0],$coords[1]);
        }
    }
}
//$a = new iNewton();
//$a->dibujarTablaDiferenciasDivididas();
//echo "<br>";
//echo "<br>";
//$a->calcularCoeficientesAj();

?>